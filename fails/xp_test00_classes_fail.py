import multiprocessing as mp
import threading as thr
import queue
import time
import functools
import psutil
from xplane.xpinterfaces import NavInterface, APInterface

#architecture based on sketch_03

#TODO:
#add logging
#create generic thread subclasses read_from_IO, send_to_IO
#add a third, CPU-intensive process allocated to another core


def timing(f):

    @functools.wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Call to {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap


class CustomProcess(mp.Process):

    def __init__(self, *, affinity:list=None, priority=None, **kwargs):

        super().__init__(**kwargs)
        self._affinity = affinity
        self._priority = priority

    def run(self):

        proc = psutil.Process()
        if self._affinity is not None:
            proc.cpu_affinity(self._affinity)
        if self._priority is not None:
            proc.nice(self._priority)
        print(f"Starting process {self.name} with PID {proc.pid}, " +
              f"CPU affinity: {proc.cpu_affinity()}, priority: {proc.nice()}")

        if self._target:
            self._target(*self._args, **self._kwargs)


class IONav(thr.Thread):

    def __init__(self, q_output: mp.Queue, shutdown: mp.Event, **kwargs):

        super().__init__(**kwargs)
        self._output = q_output
        self._shutdown = shutdown
        self._interface = NavInterface()

    @timing
    def get_state_vector(self):
        sv = self._interface.get_state_update(('lat',))
        return sv
        # for x in range(2000000):
        #     dummy = x**2

    def run(self):

        thread_name = thr.current_thread().name

        dt = 1.0
        i = 0
        t_last = t_previous = time.perf_counter()
        while not self._shutdown.is_set():

            t_current = time.perf_counter()
            print(f"\n{thread_name}: Iteration {i}, " +
                  f"actual delta time: {t_current - t_previous}")
            t_previous = t_current

            sv = self.get_state_vector()

            try:
                self._output.put(sv, timeout=3)
                print(f"{thread_name}: Pushed a state vector update to queue {self._output}, " +
                    f"current size: {self._output.qsize()}")
            except queue.Full:
                print(f"{thread_name}: Timed out while waiting for a slot " +
                      "in the output queue")
                break

            t_next = t_last + dt
            try:
                time.sleep(t_next - time.perf_counter())
            except ValueError: #negative sleep time
                print(f"{thread_name}: PANIC!!! Missed state vector update epoch!" +
                      "We're lagging behind!")
            t_last = t_next
            i += 1

        print(f"{thread_name}: Exiting...")


class IOAutopilot(thr.Thread):

    def __init__(self, q_input: mp.Queue, shutdown: mp.Event, **kwargs):

        super().__init__(**kwargs)
        self._input = q_input
        self._shutdown = shutdown
        self._interface = APInterface()

    def configure_AP(self):
        self._interface.AP_enable()
        self._interface.enable_override()
        self._interface.enable_autothrottle()
        self._interface.select_mode(self._interface.LatMode.BANK_HOLD)
        self._interface.select_mode(self._interface.VertMode.CLIMB_RATE)

    def run(self):

        thread_name = thr.current_thread().name

        self.configure_AP()

        while not self._shutdown.is_set():

            n_pending = self._input.qsize()
            if n_pending > 1:
                print(f"{thread_name}: PANIC! {n_pending} pending autopilot command updates, "
                    + "expected at most 1. We are lagging behind!")
                while self._input.qsize() > 1: #discard obsolete messages
                    _ = self._input.get_nowait()

            try:
                #block until a new autopilot command arrives
                act = self._input.get(timeout=3)
                print(f"{thread_name}: Popped an autopilot command update from queue {self._input}, " +
                    f"current size: {self._input.qsize()}")
                if act is None: #this is a signal from the main process to stop waiting
                    print(f"{thread_name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{thread_name}: Timed out while waiting for an autopilot command update")
                break

            print(f"{thread_name}: Got the following autopilot command update: {act}")
            self._interface.set_ias(act['IAS'], units='kts')
            self._interface.set_climb_rate(act['climb_rate'], units='fpm')
            self._interface.set_bank_angle(act['bank_angle'], units='deg')

        print(f"{thread_name}: Exiting...")


class ControlMain(thr.Thread):

    def __init__(self, q_input: mp.Queue, q_output: mp.Queue, shutdown: mp.Event,
                 **kwargs):

        super().__init__(**kwargs)
        self._input = q_input
        self._output = q_output
        self._shutdown = shutdown

    @timing
    def compute_actuation_commands(self, sv):
        #increase range arbitrarily to simulate longer data processing
        for x in range(4000000):
            dummy = x ** 0.5
        # #output to FMS: # alt, lon, alt # xi_g = np.atan2(v_NED[1], v_NED[0]) # TAS # vw_NED

        # #input demands from FMS # climb_rate/h, TAS, xi_g_dot

        ap_demands = {'IAS': 100, 'climb_rate': -100, 'bank_angle': 5}
        return ap_demands


    def run(self):

        thread_name = thr.current_thread().name

        while not self._shutdown.is_set():

            n_pending = self._input.qsize()
            if n_pending > 1:
                print(f"{thread_name}: PANIC! {n_pending} pending state vector updates, "
                    + "expected at most 1. We are lagging behind!")
                while self._input.qsize() > 1: #drop obsolete state vector messages
                    _ = self._input.get_nowait()

            try:
                #block until a sv update arrives
                sv = self._input.get(timeout=3)
                print(f"{thread_name}: Popped a state vector update from queue {self._input}, " +
                    f"current size: {self._input.qsize()}")
                if sv is None: #this is a signal from the main process to stop
                    print(f"{thread_name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{thread_name}: Timed out while waiting for a state vector update")
                break

            #if no update is available, this won't run
            print(f"{thread_name}: Got the following state vector update: {sv}")
            act = self.compute_actuation_commands(sv)

            try:
                self._output.put(act, timeout=3)
                print(f"{thread_name}: Pushed an autopilot command update to queue {self._output}, " +
                    f"current size: {self._output.qsize()}")
            except queue.Full:
                print(f"{thread_name}: Timed out while waiting for a free autopilot command queue slot")
                break

        print(f"{thread_name}: Exiting...")


def main_function(time_to_run=None):

    def start_processes():
        for p in processes.values():
            p.start()

    def stop_processes():
        shutdown.set()
        #release any processes blocked on empty queues
        for q in queues.values():
            if q.qsize()==0:
                q.put(None, timeout=3)

    def run_IO():
        threads_IO = {
            'IONav': IONav(name='I/O Nav', q_output=queues['Nav2Ctl'],
                        shutdown=shutdown),
            'IOAct': IOAutopilot(name='I/O AP', q_input=queues['Ctl2Act'],
                                shutdown=shutdown)}
        for thread in threads_IO.values():
            thread.start()

    def run_Control():
        threads_Ctl = {
            'ControlMain':
                ControlMain(name='ControlMain', q_input=queues['Nav2Ctl'],
                            q_output=queues['Ctl2Act'], shutdown=shutdown)}
        for thread in threads_Ctl.values():
            thread.start()

    shutdown = mp.Event()

    queues = {'Nav2Ctl': mp.Queue(), 'Ctl2Act': mp.Queue()}

    processes = {
        'IO': CustomProcess(affinity=[0], target=run_IO, name='I/O'),
        'Ctl': CustomProcess(affinity=[2], target=run_Control, name='Control')}

    start_processes()
    if time_to_run:
        time.sleep(time_to_run)
    print(f"\n{mp.current_process().name}: Shutting down...")
    stop_processes()


if __name__ == "__main__":
    main_function(30.0)