# pylint: disable= invalid-name, non-ascii-name
from enum import IntEnum
from abc import ABC, abstractmethod
import numpy as np
import environment.WGS84 as WGS84

π = np.pi
two_π = 2*π

class Primitive2D(ABC):
    
    @abstractmethod
    def length(self):
        pass
    
    @abstractmethod
    def sample(self, n_per_m):
        pass

    @abstractmethod
    def guidance(self, vehicle_position: WGS84.Point) -> np.ndarray:
        pass
    
    @abstractmethod
    def exit_condition(self, vehicle_position: WGS84.Point) -> bool:
        pass
    
    def get_scale_factors(self, ref: WGS84.Point):
        (M, N) = ref.radii
        return np.array((M + ref.h, (N + ref.h) * np.cos(ref.lat)))

class CircularArc(Primitive2D):

    _ε_angle = 1e-10

    class TurnSense(IntEnum):
        CW = 1
        CCW = -1

    def __init__(self, O: WGS84.Point, R: float, ψ_0: float, ψ_1: float, 
                 turn_sense: CircularArc.TurnSense):

        #both angles must be wrapped to [0, two_pi]
        self.O = np.array(O, dtype='float')
        self.R = R
        self.ψ_0 = np.mod(ψ_0, two_π)
        self.ψ_1 = np.mod(ψ_1, two_π)
        self.turn_sense = turn_sense
        self._scale_factors = super().get_scale_factors(self.O)

    @property
    def Δψ(self):
        return np.fmod(self.ψ_1 - self.ψ_0 + two_π * self.turn_sense, two_π)

    @property
    def angle(self):
        return abs(self.Δψ)

    @property
    def P_start(self):
        return self.O + self.R * np.array([np.cos(self.ψ_0), np.sin(self.ψ_0)])

    @property
    def P_end(self):
        return self.O + self.R * np.array([np.cos(self.ψ_1), np.sin(self.ψ_1)])

    def __bool__(self):

        null = bool(abs(self.Δψ) < CircularArc._ε_angle)
        cyclic = bool(abs((abs(self.Δψ) - 2 * π)) < CircularArc._ε_angle)

        return not null or cyclic

    def length(self):
        return self.R * abs(self.Δψ)

    def sample(self, n_per_m: float):
        raise(NotImplementedError)
        n = 2 + np.int(np.floor(n_per_m * self.length))
        ψ_array = self.ψ_0 + np.linspace(0., self.Δψ, n)
        return self.O[:, np.newaxis] + self.R * np.vstack((np.cos(ψ_array),
                                                           np.sin(ψ_array)))


class LineSegment(Primitive2D):

    _ε_length = 1e-10

    def __init__(self, P_start: WGS84.Point, P_end: WGS84.Point):

        self.P_start = np.array(P_start, dtype='float')
        self.P_end = np.array(P_end, dtype='float')
        self._scale_factors = 0.5 * (
                                super().get_scale_factors(self.P_start) +
                                super().get_scale_factors(self.P_end) )
                                       
        
    def __bool__(self):
        return bool(self.length() > LineSegment._ε_length)

    def length(self):
        raise(NotImplementedError)
        return np.linalg.norm(self.P_end - self.P_start)
    
    def sample(self, n_per_m: float):
        raise(NotImplementedError)
        n = 2 + np.int(np.floor(n_per_m * self.length))
        s = np.tile(np.linspace(0., 1., n), [2, 1])
        P_start = self.P_start[:, np.newaxis]
        P_end = self.P_end[:, np.newaxis]
        return P_start + (P_end - P_start) * s
