import dubins as db
import numpy as np
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"

WP_in = db.Waypoint2D(r = np.array([1,0]), v = np.array([1,0]))
WP_out = db.Waypoint2D(r = np.array([1,3]), v = np.array([1,0]))

RSR = db.build_Dubins(WP_in, WP_out, 1, "RSR")

RSR[0].length
RSR[1].length
RSR[2].length
np.array([sum([section.length for section in RSR])])

RLRp = db.build_Dubins(WP_in, WP_out, 1, "RLR+")

RLRp[0].length
RLRp[1].length
RLRp[2].length
np.array([sum([section.length for section in RLRp])])

RLRn = db.build_Dubins(WP_in, WP_out, 1, "RLR-")

RLRn[0].length
RLRn[1].length
RLRn[2].length
np.array([sum([section.length for section in RLRn])])

LSR = db.build_Dubins(WP_in, WP_out, 1, "LSR")
print([section.length for section in LSR])
print(np.array([sum([section.length for section in LSR])]))