import numpy as np
from environment import WGS84

class Waypoint():
    
    def __init__(self, location: WGS84.Point, velocity: np.ndarray):
        
        self.location = location
        self.velocity = velocity