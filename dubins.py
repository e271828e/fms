# pylint: disable= invalid-name, non-ascii-name
from collections import namedtuple
from enum import IntEnum
import numpy as np
from primitives import CircularArc, LineSegment
from waypoint import Waypoint

π = np.pi
_ε_dist = 1e-10

Constraint = namedtuple('Constraint', ['r', 't'])

class Path:
    #self.WP_in
    #self.WP_out
    #self.sections
    #sample()
    #length()
    pass
      
class PatternSense(IntEnum):
    CW = 1
    CCW = -1

class PatternType(IntEnum):
    A = 1
    B = -1
    
class Dubins(Path):

    def __init__(self):
        #call super().__init__ with received args, then fill the sections
        #attribute calling get_sections. see if these should be static methods
        #or they can stay outside the class. the same decision applies to the
        #enumerations. como no va a haber mas clases relevantes en este modulo,
        #no parece un problema que se queden fuera
        
    pass

#en principio parece que la clase Dubins va a necesitar estados internos para la
#captura? O cada uno de las secciones activas puede limitarse a comprobar su
#propia condicion de captura una vez se active?
#cuanto el arco sea de mas de 180 grados fallara.
#la forma seria calcular el delta_psi desde el psi_0 del arco hasta el psi
#actual con la misma cuenta que usamos a la hora de construir un arco;
# si el delta_psi resultante es mayor que el del arco, hemos terminado.

#por tanto, el arco de circulo es una primitiva con su propia condicion de
#captura, pero a la hora de asignarle una guidance law podemos definir una sola
#circular guidance law que valga tanto para arcos como para espirales, ascensos,
#loiters, etc. y cada uno sera una primitiva.

#si que parece razonable que Dubins sea una clase. llamara a super(), que asigna
#los WPs que nos pasan, 


def get_sections(WP_in: Waypoint, WP_out: Waypoint, R: float,
                pattern: str = None):

    C_in = Constraint(WP_in.r, WP_in.v / np.linalg.norm(WP_in.v))
    C_out = Constraint(WP_out.r, WP_out.v / np.linalg.norm(WP_out.v))

    if pattern is None:
        pattern = 'shortest'
    
    if pattern == 'shortest':
        candidates = (pattern['f'](C_in, C_out, R, *pattern['args'])
                        for pattern in pattern_dict.values())
        candidates = [filter(None, candidates)]
        lengths = np.array([sum([section.length() for section in candidate])
                            for candidate in candidates] )
        return candidates[np.argmin(lengths)]

    try:
        return pattern_dict[pattern]['f'](
            C_in, C_out, R, *pattern_dict[pattern]['args'])
    except KeyError:
        print(f"{pattern} is not a valid pattern")
        return None

    #path_sense = CW when the turn sense of the entry arc is CW 
    #path_type = CW when the angle γ measured from v_io to Om is CW
        
def get_arc_params(C: Constraint, R: float, sense: CircularArc.TurnSense):

    r_C, t_C = C
    n_C = sense * np.array((-t_C[1], t_C[0]))
    O = r_C + R * n_C
    ψ_0 = np.arctan2(-n_C[1], -n_C[0])

    return O, ψ_0


def get_io_params(O_in: np.ndarray, O_out: np.ndarray):
  
    x_io = O_out - O_in
    d_io = np.linalg.norm(x_io)
    ψ_io = np.arctan2(x_io[1], x_io[0])

    return d_io, ψ_io
  

def CSC(C_in: Constraint, C_out: Constraint, R: float,
        p_sense: PatternSense):

    #if this fails, either cast to int or pass directly p_sense to arc constructors
    arc_in_sense = CircularArc.TurnSense(p_sense)
    arc_out_sense = CircularArc.TurnSense(p_sense)
  
    O_in, ψ_in_0 = get_arc_params(C_in, R, p_sense)
    O_out, ψ_out_1 = get_arc_params(C_out, R, p_sense)
    _, ψ_io = get_io_params(O_in, O_out)
  
    ψ_in_1 = ψ_out_0 = ψ_io - p_sense * π/2

    arc_in = CircularArc(O_in, R, ψ_in_0, ψ_in_1, p_sense)
    arc_out = CircularArc(O_out, R, ψ_out_0, ψ_out_1, p_sense)
    seg_mid = LineSegment(arc_in.P_end, arc_out.P_start)

    return [section for section in (arc_in, seg_mid, arc_out) if section]


def CSCr(C_in: Constraint, C_out: Constraint, R: float,
        p_sense: PatternSense):

    O_in, ψ_in_0 = get_arc_params(C_in, R, p_sense)
    O_out, ψ_out_1 = get_arc_params(C_out, R, -p_sense)
    d_io, ψ_io = get_io_params(O_in, O_out)

    if d_io < (2 + _ε_dist):
        return None

    δ = np.arcsin(2*R / d_io)
    ψ_in_1 = ψ_io + p_sense * (δ - π/2)
    ψ_out_0 = ψ_io + p_sense * (δ + π/2)

    arc_in = CircularArc(O_in, R, ψ_in_0, ψ_in_1, p_sense)
    arc_out = CircularArc(O_out, R, ψ_out_0, ψ_out_1, -p_sense)
    seg_mid = LineSegment(arc_in.P_end, arc_out.P_start)

    return [section for section in (arc_in, seg_mid, arc_out) if section]
    

def CCrC(C_in: Constraint, C_out: Constraint, R: float,
        p_sense: PatternSense, p_type: PatternType):
    
    O_in, ψ_in_0 = get_arc_params(C_in, R, p_sense)
    O_out, ψ_out_1 = get_arc_params(C_out, R, p_sense)
    d_io, ψ_io = get_io_params(O_in, O_out)
    
    if d_io > (4*R + _ε_dist):
        return None

    γ = p_type * np.arccos(d_io / (4*R))

    ψ_in_1 = ψ_io + γ
    ψ_mid_0 = ψ_in_1 + π

    ψ_mid_1 = ψ_io - γ
    ψ_out_0 = ψ_mid_1 + π

    O_mid = O_in + 2*R * np.array([np.cos(ψ_in_1), np.sin(ψ_in_1)])

    arc_in = CircularArc(O_in, R, ψ_in_0, ψ_in_1, p_sense)
    arc_out = CircularArc(O_out, R, ψ_out_0, ψ_out_1, p_sense)
    arc_mid = CircularArc(O_mid, R, ψ_mid_0, ψ_mid_1, -p_sense)

    return [section for section in (arc_in, arc_mid, arc_out) if section]

pattern_dict = {
    'RSR': {'f': CSC, 'args': (PatternSense.CW,)},
    'LSL': {'f': CSC, 'args': (PatternSense.CCW,)},
    'RSL': {'f': CSCr, 'args': (PatternSense.CW,)},
    'LSR': {'f': CSCr, 'args': (PatternSense.CCW,)},
    'RLR+': {'f': CCrC, 'args': (PatternSense.CW, PatternType.A)},
    'RLR-': {'f': CCrC, 'args': (PatternSense.CW, PatternType.B)},
    'LRL+': {'f': CCrC, 'args': (PatternSense.CCW, PatternType.A)},
    'LRL-': {'f': CCrC, 'args': (PatternSense.CCW, PatternType.B)}
}

#alternative for function storage
# pattern_dict = {
#     'RSR': lambda C_in, C_out, R: CSC(C_in, C_out, R, PatternSense.CW),
#called as: pattern_dict['RSR'](C_in, C_out, R)

#yet another alternative is to place this within __init__, where C_in, C_out and
#R are already defined, so they are captured by the lambda definition. then:
# patterns = {
#     'RSR': lambda: CSC(C_in, C_out, R, PatternSense.CW),
    # 'RLR+': lambda: CCrC(C_in, C_out, R, PatternSense.CW, PatternType.A),
#called as: patterns['RSR']
#and used as:
# candidates = [filter(None, (patterns[p]() for p in patterns.values()))]

#the reason for building the functions is allowing to ask for a specific
#pattern. once we are always computing the optimal pattern, we know we will have
#to evaluate all of them, so we could simply call the functions rather than
#defining lambdas.

if __name__ == '__main__':
    print ("Running as a script")