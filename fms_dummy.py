import socket
import json
import time

MAX_BYTES = 4096

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('127.0.0.1', 1060))
print('Listening at {}'.format(sock.getsockname()))

while True:
    message_bytes, address = sock.recvfrom(MAX_BYTES)
    message_str = message_bytes.decode('ascii')
    message_dict = json.loads(message_str)
    print(f"\n Received message from client at {address}, " +
          f"here's what the JSON looks like: \n {message_str}")

    #sleep for a while before responding
    time.sleep(2)
    response_dict = {'TAS': 40, 'hdot': 2, 'phi': -0.1} #SI units!
    response_str = json.dumps(response_dict)
    response_bytes = response_str.encode('ascii')
    print(f"\n And this is what the response string should look like: \n {response_str}")
    sock.sendto(response_bytes, address)