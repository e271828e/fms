import multiprocessing as mp
import threading as thr
import queue
import time
import functools
import psutil


def timing(f):

    @functools.wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Call to {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap


class myProcess(mp.Process):

    def __init__(self, affinity:list=None, priority=None,
                 **kwargs):

        super().__init__(**kwargs)
        self._affinity = affinity
        self._priority = priority

    def run(self):
        proc = psutil.Process()
        if self._affinity is not None:
            proc.cpu_affinity(self._affinity)
        if self._priority is not None:
            proc.nice(self._priority)
        print(f"Starting process {self.name} with PID {proc.pid}, " +
              f"CPU affinity: {proc.cpu_affinity()}, priority: {proc.nice()}")


class IOProcess(myProcess):

    def __init__(self, from_Ctl: mp.Queue, to_Ctl: mp.Queue,
                 shutdown: mp.Event, **kwargs):

        super().__init__(**kwargs)
        self._from_Ctl = from_Ctl
        self._to_Ctl = to_Ctl
        self._shutdown = shutdown

    def run(self):
        super().run()

        threads = {'NavIO': thr.Thread(name='NavIO', target=self.f_NavIO,
                                       args=(self._to_Ctl, self._shutdown)),
                        'ActIO': thr.Thread(name='ActIO', target=self.f_ActIO,
                                       args=(self._from_Ctl, self._shutdown))}

        for thread in threads.values():
            thread.start()

    @staticmethod
    def f_NavIO(to_Ctl: mp.Queue, shutdown: mp.Event):

        @timing
        def get_state_vector():
            for x in range(2000000):
                data = x**2
            return data

        thread_name = thr.current_thread().name

        dT = 1.0
        i = 0
        T_last = t_previous = time.perf_counter()
        while not shutdown.is_set():

            t_current = time.perf_counter()
            print(f"\n{thread_name}: Iteration {i}, " +
                  f"actual delta time: {t_current - t_previous}")
            t_previous = t_current

            sv = get_state_vector()

            try:
                to_Ctl.put(sv, timeout=3)
                print(f"{thread_name}: Pushed a state vector update to queue {to_Ctl}, " +
                    f"current size: {to_Ctl.qsize()}")
            except queue.Full:
                print(f"{thread_name}: Timed out while waiting for a slot " +
                      "in the output queue")
                break

            T_next = T_last + dT
            try:
                time.sleep(T_next - time.perf_counter())
            except ValueError: #negative sleep time
                print(f"{thread_name}: PANIC!!! Missed state vector update epoch!" +
                      "We're lagging behind!")
            T_last = T_next
            i += 1

        print(f"{thread_name}: Exiting...")


    @staticmethod
    def f_ActIO(from_Ctl: mp.Queue, shutdown: mp.Event):

        thread_name = thr.current_thread().name

        while not shutdown.is_set():

            n_pending = from_Ctl.qsize()
            if n_pending > 1:
                print(f"{thread_name}: PANIC! {n_pending} pending actuator command updates, "
                    + "expected at most 1. We are lagging behind!")
                while from_Ctl.qsize() > 1: #discard obsolete messages
                    _ = from_Ctl.get_nowait()

            try:
                #block until a new actuator command arrives
                act = from_Ctl.get(timeout=3)
                print(f"{thread_name}: Popped an actuator command update from queue {from_Ctl}, " +
                    f"current size: {from_Ctl.qsize()}")
                if act is None: #this is a signal from the main process to stop waiting
                    print(f"{thread_name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{thread_name}: Timed out while waiting for an actuator command update")
                break

            #eventually do something with act

        print(f"{thread_name}: Exiting...")


class CtlProcess(myProcess):

    def __init__(self, from_IO: mp.Queue, to_IO: mp.Queue,
                 shutdown: mp.Event, **kwargs):
        super().__init__(**kwargs)
        self._from_IO = from_IO
        self._to_IO = to_IO
        self._shutdown = shutdown


    def run(self):

        super().run()
        threads = {'Ctl': thr.Thread(name='Ctl', target=self.f_Ctl,
                                     args=(self._from_IO, self._to_IO, self._shutdown))}

        for thread in threads.values():
            thread.start()


    @staticmethod
    def f_Ctl(from_IO: mp.Queue, to_IO: mp.Queue, shutdown: mp.Event):

        @timing
        def compute_actuation_commands(sv):
            #increase range arbitrarily to simulate longer data processing
            for _ in range(1000000):
                act = sv ** 0.5
            return act

        thread_name = thr.current_thread().name

        while not shutdown.is_set():

            n_pending = from_IO.qsize()
            if n_pending > 1:
                print(f"{thread_name}: PANIC! {n_pending} pending state vector updates, "
                    + "expected at most 1. We are lagging behind!")
                while from_IO.qsize() > 1: #drop obsolete state vector messages
                    _ = from_IO.get_nowait()

            try:
                #block until a sv update arrives
                sv = from_IO.get(timeout=3)
                print(f"{thread_name}: Popped a state vector update from queue {from_IO}, " +
                    f"current size: {from_IO.qsize()}")
                if sv is None: #this is a signal from the main process to stop
                    print(f"{thread_name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{thread_name}: Timed out while waiting for a state vector update")
                break

            #if no update is available, this won't run
            act = compute_actuation_commands(sv)

            try:
                to_IO.put(act, timeout=3)
                print(f"{thread_name}: Pushed an actuator command update to queue {to_IO}, " +
                    f"current size: {to_IO.qsize()}")
            except queue.Full:
                print(f"{thread_name}: Timed out while waiting for a free actuation queue slot")
                break

        print(f"{thread_name}: Exiting...")


def main_function(time_to_run=None):

    def start_processes():
        for p in processes.values():
            p.start()

    def stop_processes():
        shutdown.set()
        #release any processes blocked on empty queues
        for q in queues.values():
            if q.qsize()==0:
                q.put(None, timeout=3)

    shutdown = mp.Event()

    queues = {'IO_Ctl': mp.Queue(), 'Ctl_IO': mp.Queue()}

    processes = {'IO': IOProcess(queues['Ctl_IO'], queues['IO_Ctl'], name='I/O',
                                 shutdown=shutdown, affinity=[2]),
                 'Ctl': CtlProcess(queues['IO_Ctl'], queues['Ctl_IO'], name='Control',
                                   shutdown=shutdown, affinity=[4])}

#TODO: create threads within IO:

    #add logging
    #create generic thread subclasses read_from_IO, send_to_IO
    #try to replace subclassing by run methods

    start_processes()

    if time_to_run:
        time.sleep(time_to_run)

    print(f"\n{mp.current_process().name}: Shutting down...")
    stop_processes()

if __name__ == "__main__":
    main_function(5.0)