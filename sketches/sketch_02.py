import multiprocessing as mp
import threading
import queue
import time
import functools
import psutil


def timing(f):

    @functools.wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Call to {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output

    return wrap

# def with_affinity(affinity_list):
#     def wrapped_affinity(f):
#         def wrapper(*args, **kwargs):
#             proc = psutil.Process()
#             proc.cpu_affinity = [affinity_list]
#             print(f"Running process {proc.pid} with affinity {affinity_list}")
#             output = f(*args, *kwargs)
#             return output
#         return wrapper
#     return wrapped_affinity

class myProcess(mp.Process):

    def __init__(self, affinity:list=None, priority=None,
                 **kwargs):

        super().__init__(**kwargs)
        self._affinity = affinity
        self._priority = priority

    def run(self):
        proc = psutil.Process()
        if self._affinity is not None:
            proc.cpu_affinity(self._affinity)
        if self._priority is not None:
            proc.nice(self._priority)
        print(f"Starting process {self.name} with PID {proc.pid}, " +
              f"CPU affinity: {proc.cpu_affinity()}, priority: {proc.nice()}")


class IOProcess(myProcess):

    def __init__(self, q_from_ctl: mp.Queue, q_to_ctl: mp.Queue,
                 shutdown: mp.Event, **kwargs):
        super().__init__(**kwargs)
        self._dT = 1.0
        self._q_from_ctl = q_from_ctl
        self._q_to_ctl = q_to_ctl
        self._shutdown = shutdown

    @timing
    def get_state_vector(self):
        for x in range(2000000):
            data = x**2
        return data

    def run(self):
        super().run()

        T_last = t_previous = time.perf_counter()
        i = 0

        while not self._shutdown.is_set():

            t_current = time.perf_counter()
            print(f"\n{self.name}: Iteration {i}, " +
                  f"actual delta time: {t_current - t_previous}")
            t_previous = t_current

            #Actuation commands are not consumed in this sketch, so q_from_ctl
            #is expected to grow
            # print(f"{self.name}: Pending actuation commands:" +
            #       f"{self._q_from_ctl.qsize()}")

            sv = self.get_state_vector()

            try:
                print(f"{self.name}: Pushing an item to queue {self._q_to_ctl}, " +
                    f"current size: {self._q_to_ctl.qsize()}")
                self._q_to_ctl.put(sv, timeout=3)
            except queue.Full:
                print(f"{self.name}: Timed out while waiting for a slot " +
                      "in the output queue")

            T_next = T_last + self._dT
            try:
                time.sleep(T_next - time.perf_counter())
            except ValueError: #negative sleep time
                print(f"{self.name}: PANIC!!! Missed state vector update epoch!" +
                      "We're lagging behind!")
            T_last = T_next
            i += 1

        print(f"{self.name}: Exiting...")



class CtlProcess(myProcess):

    def __init__(self, q_from_io: mp.Queue, q_to_io: mp.Queue,
                 shutdown: mp.Event, **kwargs):
        super().__init__(**kwargs)
        self._q_from_io = q_from_io
        self._q_to_io = q_to_io
        self._shutdown = shutdown

    @timing
    def compute_actuation_commands(self, sv):
        #increase range arbitrarily to simulate longer data processing
        print(f"{self.name}: Computing actuation commands...")
        for _ in range(1000000):
            act = sv ** 0.5
        print(f"{self.name}: ... done")
        return act

    def run(self):

        super().run()

        while not self._shutdown.is_set():

            n_pending = self._q_from_io.qsize()
            if n_pending > 1:
                print(f"{self.name}: PANIC! {n_pending} pending state vector updates, "
                    + "expected at most 1. We are lagging behind!")

            #if we are lagging behind, we can keep popping from the queue,
            #overwriting each state vector update with a more recent one until
            #the queue is empty. obviously, this is dealing with the symptom,
            #not the underlying cause, which is that we are taking to long to
            #process the updates
            while self._q_from_io.qsize() > 1:
                _ = self._q_from_io.get_nowait()

            try:
                #block until a sv update arrives
                sv = self._q_from_io.get(timeout=3)
                if sv is None: #this is a signal from the main process to stop waiting
                    print(f"{self.name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{self.name}: Timed out while waiting for a state vector update")

            act = self.compute_actuation_commands(sv) #if no update available, use the previous one

            print(f"{self.name}: Pushing an item to queue {self._q_to_io}, " +
                f"current size: {self._q_to_io.qsize()}")

            try:
                self._q_to_io.put(act, timeout=3)
            except queue.Full:
                print(f"{self.name}: Timed out while waiting for a free actuation queue slot")


        print(f"{self.name}: Exiting...")


def main_function(time_to_run=None):

    def close_queues():
        for q in queues.values():
            try:
                q.put(None, timeout=3) #TODO: handle the full queue case...
            except queue.Full:
                print(f"{mp.current_process().name}: Timed out while trying " +
                      "to push None value into queue")
            q.close()

    def start_processes():
        for p in processes.values():
            p.start()

    def stop_processes():
        shutdown.set()

    shutdown = mp.Event()

    queues = {'IO_Ctl': mp.Queue(),
              'Ctl_IO': mp.Queue()}
    processes = {'IO': IOProcess(queues['Ctl_IO'], queues['IO_Ctl'], name='I/O',
                                 shutdown=shutdown, affinity=[2]),
                 'Ctl': CtlProcess(queues['IO_Ctl'], queues['Ctl_IO'], name='Control',
                                   shutdown=shutdown, affinity=[4])}

#TODO: create threads within IO:
    # thread 1: initially, generate synthetic periodic sv updates. put them in
    #q_to_ctl. in the future, this will be replaced by a XPlaneConnect instance
    #that connects to xplane and either configures it to receive periodic state
    #vector updates or requests them periodically. regardless, then there will
    #be no q_nav_in queue. instead it will have socket.recv() whose output will
    #be formatted and put into q_to_ct

    # thread 2: get act from q_from_ctl, send it to XPlane via XPlaneConnect.
    # this thread will need to hold another XPlaneConnect instance

    #add logging

    start_processes()

    if time_to_run:
        time.sleep(time_to_run)

    print(f"\n{mp.current_process().name}: Shutting down...")
    stop_processes()
    close_queues()

if __name__ == "__main__":
    main_function(5.0)