import threading
import queue
import time
from functools import wraps
from itertools import count
from typing import Iterable

"""This makes each thread execute its processing intensive section to completion
 by acquiring a shared lock before entering it. This is a VERY BAD idea in
 general, because of blocking IO, and because a thread taking to long to do its
 thing would mess up the scheduling for all others"""

def timing(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"{f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap

class Producer(threading.Thread):

    def __init__(self, q_iter:Iterable, ex_lock:threading.Lock):
        super().__init__(daemon=True)
        self._q_iter = q_iter
        self._ex_lock = ex_lock
        self._dT = 2.0

    @timing
    def generate_data(self):
        for x in range(100000):
            data = x**2
        for q in self._q_iter:
            q.put(data)
            print(f"{self.name}: Pushed an item to queue {q}, current size: {q.qsize()}")

    def run(self):
        T_last = t_previous = time.perf_counter()

        for i in count(0):

            t_current = time.perf_counter()
            print(f"Iteration {i}, actual delta time: {t_current - t_previous}")
            t_previous = t_current

            #exclusive CPU use
            with self._ex_lock:
                self.generate_data()

            T_next = T_last + self._dT
            time.sleep(T_next - time.perf_counter())
            T_last = T_next


class Consumer(threading.Thread):

    def __init__(self, q:queue.Queue, ex_lock:threading.Lock):
        super().__init__(daemon=True)
        self._q = q
        self._ex_lock = ex_lock

    @timing
    def process_data(self, data):
        #increase range arbitrarily to simulate longer data processing and watch
        #CPU load increase from 0 to 1/n_cores
        print(f"{self.name}: Processing data...")
        for _ in range(100000):
            data = data ** 0.5
        print(f"{self.name}: ... done")

    def run(self):

        while True:
            if self._q.qsize() == 0: #previous data processing should've finished by now
                print(f"{self.name}: OK, the queue is empty, I'm blocking")
            elif self._q.qsize() == 1:
                print(f"{self.name}: OK, got one element waiting in the queue")
            else: #we are lagging behind!
                print(f"{self.name}: Queue already has {self._q.qsize()} element(s), I'm lagging behind!")

            data = self._q.get()
            #demand exclusive CPU use before processing data (no other thread
            #will be able to interrupt this once the lock is acquired)
            with self._ex_lock:
                self.process_data(data)


def main_function(time_to_run=None):

    q0 = queue.Queue(10)
    q1 = queue.Queue(10)

    #this lock allows each thread to run its data generation or processing
    #section to completion without execution being transferred to any other
    #consumer. this ensure that the measured data processing time is accurate
    #for all threads. otherwise, the data processing section for a thread
    #could be interrupted to execute another, and its measured time would be its
    #own plus the other one's! note that this would be a VERY BAD idea if the
    #generating thread were actually IO bound, because it would block, defeating
    #the whole purpose of threading.
    ex_lock = threading.Lock()
    producer = Producer((q0, q1), ex_lock)
    consumer0 = Consumer(q0, ex_lock)
    consumer1 = Consumer(q1, ex_lock)
    producer.start()
    consumer0.start()
    consumer1.start()

    #allow forever-running daemon threads to run only for the specified time.
    #after that, main_thread wakes up, exits and kills all daemon threads along
    #with it
    if time_to_run:
        time.sleep(time_to_run)
    print("Exiting main thread")

#need
#1) a producer function that runs forever

if __name__ == "__main__":
    main_function(10.0)