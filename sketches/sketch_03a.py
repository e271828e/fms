import multiprocessing as mp
import threading as thr
import queue
import time
import functools
import psutil

#TODO:
#add logging
#create generic thread subclasses read_from_IO, send_to_IO


def timing(f):

    @functools.wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Call to {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap


class ScheduledProcess(mp.Process):

    def __init__(self, *, affinity:list=None, priority=None, **kwargs):

        super().__init__(**kwargs)
        self._affinity = affinity
        self._priority = priority

    def run(self):

        proc = psutil.Process()
        if self._affinity is not None:
            proc.cpu_affinity(self._affinity)
        if self._priority is not None:
            proc.nice(self._priority)
        print(f"Starting process {self.name} with PID {proc.pid}, " +
              f"CPU affinity: {proc.cpu_affinity()}, priority: {proc.nice()}")

        if self._target:
            self._target(*self._args, **self._kwargs)


def f_proc_IO(from_Ctl: mp.Queue, to_Ctl: mp.Queue, shutdown: mp.Event):

    def f_thread_IONav(to_Ctl: mp.Queue, shutdown: mp.Event):

        @timing
        def get_state_vector():
            for x in range(2000000):
                data = x**2
            return data

        thread_name = thr.current_thread().name

        dT = 1.0
        i = 0
        T_last = t_previous = time.perf_counter()
        while not shutdown.is_set():

            t_current = time.perf_counter()
            print(f"\n{thread_name}: Iteration {i}, " +
                  f"actual delta time: {t_current - t_previous}")
            t_previous = t_current

            sv = get_state_vector()

            try:
                to_Ctl.put(sv, timeout=3)
                print(f"{thread_name}: Pushed a state vector update to queue {to_Ctl}, " +
                    f"current size: {to_Ctl.qsize()}")
            except queue.Full:
                print(f"{thread_name}: Timed out while waiting for a slot " +
                      "in the output queue")
                break

            T_next = T_last + dT
            try:
                time.sleep(T_next - time.perf_counter())
            except ValueError: #negative sleep time
                print(f"{thread_name}: PANIC!!! Missed state vector update epoch!" +
                      "We're lagging behind!")
            T_last = T_next
            i += 1

        print(f"{thread_name}: Exiting...")


    def f_thread_IOAct(from_Ctl: mp.Queue, shutdown: mp.Event):

        thread_name = thr.current_thread().name

        while not shutdown.is_set():

            n_pending = from_Ctl.qsize()
            if n_pending > 1:
                print(f"{thread_name}: PANIC! {n_pending} pending actuator command updates, "
                    + "expected at most 1. We are lagging behind!")
                while from_Ctl.qsize() > 1: #discard obsolete messages
                    _ = from_Ctl.get_nowait()

            try:
                #block until a new actuator command arrives
                act = from_Ctl.get(timeout=3)
                print(f"{thread_name}: Popped an actuator command update from queue {from_Ctl}, " +
                    f"current size: {from_Ctl.qsize()}")
                if act is None: #this is a signal from the main process to stop waiting
                    print(f"{thread_name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{thread_name}: Timed out while waiting for an actuator command update")
                break

            #eventually do something with act

        print(f"{thread_name}: Exiting...")


    threads = {'IONav': thr.Thread(name='IONav', target=f_thread_IONav,
                                    args=(to_Ctl, shutdown)),
                'IOAct': thr.Thread(name='IOAct', target=f_thread_IOAct,
                                args=(from_Ctl, shutdown))}

    for thread in threads.values():
        thread.start()


def f_proc_Ctl(from_IO: mp.Queue, to_IO: mp.Queue, shutdown: mp.Event):

    def f_thread_CtlMain(from_IO: mp.Queue, to_IO: mp.Queue, shutdown: mp.Event):

        @timing
        def compute_actuation_commands(sv):
            #increase range arbitrarily to simulate longer data processing
            for _ in range(1000000):
                act = sv ** 0.5
            return act

        thread_name = thr.current_thread().name

        while not shutdown.is_set():

            n_pending = from_IO.qsize()
            if n_pending > 1:
                print(f"{thread_name}: PANIC! {n_pending} pending state vector updates, "
                    + "expected at most 1. We are lagging behind!")
                while from_IO.qsize() > 1: #drop obsolete state vector messages
                    _ = from_IO.get_nowait()

            try:
                #block until a sv update arrives
                sv = from_IO.get(timeout=3)
                print(f"{thread_name}: Popped a state vector update from queue {from_IO}, " +
                    f"current size: {from_IO.qsize()}")
                if sv is None: #this is a signal from the main process to stop
                    print(f"{thread_name}: Got a None value from the queue")
                    break
            except queue.Empty:
                print(f"{thread_name}: Timed out while waiting for a state vector update")
                break

            #if no update is available, this won't run
            act = compute_actuation_commands(sv)

            try:
                to_IO.put(act, timeout=3)
                print(f"{thread_name}: Pushed an actuator command update to queue {to_IO}, " +
                    f"current size: {to_IO.qsize()}")
            except queue.Full:
                print(f"{thread_name}: Timed out while waiting for a free actuation queue slot")
                break

        print(f"{thread_name}: Exiting...")


    threads = {'CtlMain': thr.Thread(name='CtlMain', target=f_thread_CtlMain,
                                    args=(from_IO, to_IO, shutdown))}

    for thread in threads.values():
        thread.start()


def main_function(time_to_run=None):

    def start_processes():
        for p in processes.values():
            p.start()

    def stop_processes():
        shutdown.set()
        #release any processes blocked on empty queues
        for q in queues.values():
            if q.qsize()==0:
                q.put(None, timeout=3)

    shutdown = mp.Event()

    queues = {'IO_Ctl': mp.Queue(), 'Ctl_IO': mp.Queue()}

    processes = {
        'IO': ScheduledProcess(affinity=[2], target=f_proc_IO,
                               args=(queues['Ctl_IO'], queues['IO_Ctl'], shutdown), name='I/O'),
        'Ctl': ScheduledProcess(affinity=[4], target=f_proc_Ctl,
                                args=(queues['IO_Ctl'], queues['Ctl_IO'], shutdown), name='Control')}
    start_processes()

    if time_to_run:
        time.sleep(time_to_run)

    print(f"\n{mp.current_process().name}: Shutting down...")
    stop_processes()

if __name__ == "__main__":
    main_function(5.0)