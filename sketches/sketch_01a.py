import threading
import queue
import time
from functools import wraps
from itertools import count
from typing import Iterable

"""Now we have two consumers, each with its own queue, serviced by the producer.
The problem is now that as soon as the producer copies the newly generated data
into the first consumer's queue, the consumer immediately starts processing it
before the data has been pushed to the remaining consumer queues. This might be
desirable or not. Tried to force all consumers to wait until all queues are
serviced, doesn't really work"""

def timing(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Function call {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap

def simple_scheduler(dT, f):

    T_last = t_previous = time.perf_counter()

    for i in count(0):

        t_current = time.perf_counter()
        print(f"Iteration {i}, actual delta time: {t_current - t_previous}")
        t_previous = t_current

        f()
        T_next = T_last + dT
        time.sleep(T_next - time.perf_counter())
        T_last = T_next


class Producer(threading.Thread):

    def __init__(self, q_iter:Iterable, q_updating:threading.Lock):
        super().__init__(daemon=True)
        self._q_iter = q_iter
        self._q_updating = q_updating

    @timing
    def generate_data(self):
        for x in range(100000):
            data = x**2
        for q in self._q_iter:
            q.put(data)
            print(f"{self.name}: Pushed an item to queue {q}, current size: {q.qsize()}")

    def run(self):
        simple_scheduler(dT=2.0, f=self.generate_data)


class Consumer(threading.Thread):

    def __init__(self, q:queue.Queue, q_updating:threading.Lock):
        super().__init__(daemon=True)
        self._q = q
        self._q_updating = q_updating

    @timing
    def process_data(self, data):
        #increase range arbitrarily to simulate longer data processing and watch
        #CPU load increase from 0 to 1/n_cores
        print(f"{self.name}: Processing data")
        for _ in range(100000):
            data = data ** 0.5

    def run(self):

        while True:
            print(f"{self.name}: Will now block waiting for consumer queues update")
            with self._q_updating: #block here until all consumer queues are updated
                if self._q.qsize() == 0: #previous data processing should've finished by now
                    print(f"{self.name}: OK, the queue is still empty, I'm blocking")
                elif self._q.qsize() == 1:
                    print(f"{self.name}: OK, got one element waiting in the queue")
                else: #we are lagging behind!
                    print(f"{self.name}: Queue already has {self._q.qsize()} element(s), I'm lagging behind!")

            #this should be outside the condition context manager, because if
            #for some reason this consumer's queue is empty, it would block
            #inside the context manager, preventing all other consumers from
            #accessing their own queues. however, this should not normally
            #happen
            data = self._q.get()
            print(f"{self.name}: Popped an item from queue, current size: {self._q.qsize()}")
            self.process_data(data)


def main_function(time_to_run=None):

    q0 = queue.Queue(10)
    q1 = queue.Queue(10)
    q_cond = threading.Lock()
    producer = Producer((q0, q1), q_cond)
    consumer0 = Consumer(q0, q_cond)
    consumer1 = Consumer(q1, q_cond)
    producer.start()
    consumer0.start()
    consumer1.start()

    #allow forever-running daemon threads to run only for the specified time.
    #after that, main_thread wakes up, exits and kills all daemon threads along
    #with it
    if time_to_run:
        time.sleep(time_to_run)
    print("Exiting main thread")

#need
#1) a producer function that runs forever

if __name__ == "__main__":
    main_function(10.0)