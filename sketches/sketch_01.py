import threading
import queue
import time
from functools import wraps
from itertools import count
from typing import Iterable


def timing(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"{f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap

class Producer(threading.Thread):

    def __init__(self, q_iter:Iterable):
        super().__init__(daemon=True)
        self._q_iter = q_iter
        self._dT = 2.0

    @timing
    def generate_data(self):
        for x in range(100000):
            data = x**2
        for q in self._q_iter:
            print(f"{self.name}: Pushing an item to queue {q}, current size: {q.qsize()}")
            q.put(data)

    def run(self):
        T_last = t_previous = time.perf_counter()

        for i in count(0):

            t_current = time.perf_counter()
            print(f"Iteration {i}, actual delta time: {t_current - t_previous}")
            t_previous = t_current

            self.generate_data()

            T_next = T_last + self._dT
            time.sleep(T_next - time.perf_counter())
            T_last = T_next


class Consumer(threading.Thread):

    def __init__(self, q:queue.Queue):
        super().__init__(daemon=True)
        self._q = q

    @timing
    def process_data(self, data):
        #increase range arbitrarily to simulate longer data processing and watch
        #CPU load increase from 0 to 1/n_cores
        print(f"{self.name}: Processing data...")
        for _ in range(100000):
            data = data ** 0.5
        print(f"{self.name}: ... done")

    def run(self):

        while True:
            if self._q.qsize() == 0: #previous data processing should've finished by now
                print(f"{self.name}: OK, the queue is empty, I'm blocking")
            elif self._q.qsize() == 1:
                print(f"{self.name}: OK, got one element waiting in the queue")
            else: #we are lagging behind!
                print(f"{self.name}: Queue already has {self._q.qsize()} element(s), I'm lagging behind!")

            data = self._q.get()
            self.process_data(data)


def main_function(time_to_run=None):

    q0 = queue.Queue(10)
    q1 = queue.Queue(10)

    producer = Producer((q0, q1))
    consumer0 = Consumer(q0)
    consumer1 = Consumer(q1)
    producer.start()
    consumer0.start()
    consumer1.start()

    #allow forever-running daemon threads to run only for the specified time.
    #after that, main_thread wakes up, exits and kills all daemon threads along
    #with it
    if time_to_run:
        time.sleep(time_to_run)
    print("Exiting main thread")

if __name__ == "__main__":
    main_function(10.0)