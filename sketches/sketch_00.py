import threading
import queue
import time
from functools import wraps
from itertools import count

def timing(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        t_s = time.perf_counter()
        output = f(*args, **kwargs)
        t_e = time.perf_counter()
        print(f"Function call {f.__name__}({args}, {kwargs}) took {1000*(t_e - t_s)} ms")
        return output
    return wrap

def simple_scheduler(dT, f):

    T_last = t_previous = time.perf_counter()

    for i in count(0):

        t_current = time.perf_counter()
        print(f"Iteration {i}, actual delta time: {t_current - t_previous}")
        t_previous = t_current

        f()
        T_next = T_last + dT
        time.sleep(T_next - time.perf_counter())
        T_last = T_next


def producer(q:queue.Queue):

    @timing
    def data_generator():
        for x in range(100000):
            data = x**2
        q.put(data)
        print(f"Producer: Pushed an item to queue, current size: {q.qsize()}")

    simple_scheduler(dT=2.0, f=data_generator)


def consumer0(q:queue.Queue):

    @timing
    def data_processor(data):
        #increase range arbitrarily to simulate longer data processing and watch
        #CPU load increase from 0 to 1/n_cores
        for _ in range(10000000):
            data = data ** 0.5

    while True:
        #this loop will continuously try to fetch an element from the queue, and
        #will block until producer has pushed one
        if q.empty():
            #data processing in the previous iteration should have finished
            #before the producer has had time to push another element.
            #otherwise, we are lagging behind. note that if we are still
            #processing data when the next item arrives it means that this
            #thread was hogging the CPU and it was preempted by the OS to attend
            #to the producer thread at some point
            print("Consumer 0: The queue is empty as expected, I'm about to block")
        else:
            #we are lagging behind!
            print(f"Consumer 0: Uh oh, queue already has {q.qsize()} element(s)")
        data = q.get()
        print(f"Consumer 0: Popped an item from queue, current size: {q.qsize()}")
        data_processor(data)


def main_function(time_to_run=None):

    q0 = queue.Queue(10)
    producer_thread = threading.Thread(target=producer, args=(q0,),
                                       daemon=True)
    consumer_thread = threading.Thread(target=consumer0, args=(q0,),
                                       daemon=True)
    producer_thread.start()
    consumer_thread.start()

    #allow forever-running daemon threads to run only for the specified time.
    #after that, main_thread wakes up, exits and kills all daemon threads along
    #with it
    if time_to_run:
        time.sleep(time_to_run)
    print("Exiting main thread")

#need
#1) a producer function that runs forever

if __name__ == "__main__":
    main_function(10.0)